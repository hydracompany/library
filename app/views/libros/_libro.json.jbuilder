json.extract! libro, :id, :isbn, :titulo, :paginas, :editorial, :created_at, :updated_at
json.url libro_url(libro, format: :json)
