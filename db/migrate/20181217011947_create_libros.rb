class CreateLibros < ActiveRecord::Migration[5.2]
  def change
    create_table :libros do |t|
      t.string :isbn
      t.string :titulo
      t.integer :paginas
      t.string :editorial

      t.timestamps
    end
  end
end
